<?php
global $submit_title;
global $submit_description;
global $property_price; 
global $property_label; 
global $prop_action_category;
global $prop_action_category_selected;
global $prop_category_selected;
global $property_city;
global $property_area;
global $guestnumber;
global $property_country;
global $property_description;
global $property_admin_area;
global $instant_booking;
?>


<?php if ( is_user_logged_in() ) {    ?>
<form id="new_post" name="new_post" method="post" action="" enctype="multipart/form-data" class="add-estate">
        <?php
        if (function_exists('icl_translate') ){
            print do_action( 'wpml_add_language_form_field' );
        }
        ?>
<?php }else{ ?>
<div id="new_post"  class="add-estate">
<?php } ?>
 
<div class="col-md-12">
    <div class="user_dashboard_panel">
    <h4 class="user_dashboard_panel_title"><?php  esc_html_e('Description','wpestate');?></h4>
    <div class="row">   
        <div class="alert alert-danger" id="phase1_err"></div>

        <div class="col-md-12">
            <div class="col-md-3 dashboard_chapter_label">           
                <p>
                   <label for="title"><?php esc_html_e('*Title','wpestate'); ?> </label>
                 </p>
            </div>

            <div class="col-md-6">           
                <p>
                   <label for="title"><?php esc_html_e('*Title (mandatory)','wpestate'); ?> </label>
                   <input type="text" id="title" class="form-control" placeholder="Enter Title Here" value="<?php print $submit_title; ?>" size="20" name="wpestate_title" />
                </p>
            </div>
        </div>
		<div class="col-md-12">
		 <div class="col-md-3 dashboard_chapter_label">           
                <p>
                   <label for="property_rooms"><?php esc_html_e('Number of Rooms','wpestate');?></label>
                 </p>
            </div>
			<div class="col-md-6">
				<p>
					
					<input type="text" id="numbers_of_rooms" size="40" placeholder="Enter no of rooms" class="form-control"  name="numbers_of_rooms" value="<?php print $numbers_of_rooms;?>">
				</p>
			</div>
		</div>
		<!-- Custom Fields -->
		<div class="col-md-12">	
			<div class="col-md-3 dashboard_chapter_label">           
                <p>
                   <label for="property_no_adults"><?php esc_html_e('Number of Adults','wpestate');?></label>
                 </p>
            </div>
			<div class="col-md-6">
				<p>
					
					<input type="text" id="property_no_adults" size="40" placeholder="Enter no of Adults" class="form-control"  name="property_no_adults" value="<?php print $property_no_adults;?>">
				</p>
			</div>
		</div>
		<div class="col-md-12">	
			<div class="col-md-3 dashboard_chapter_label">           
                <p>
                   <label for="property_no_child"><?php esc_html_e('Number of Children','wpestate');?></label>
                 </p>
            </div>
			<div class="col-md-6">
				<p>
					
					<input type="text" id="property_no_child" size="40" placeholder="Enter number of children" class="form-control"  name="property_no_child" value="<?php print $property_no_child;?>">
				</p>
			</div>
		</div>
		<div class="col-md-12">	
			<div class="col-md-3 dashboard_chapter_label">           
                <p>
                   <label for="property_price"><?php esc_html_e('Orignal Price','wpestate');?></label>
                 </p>
            </div>
			<div class="col-md-6">
				<p>
					
					<input type="text" id="property_price" size="40" placeholder="Enter orignal price" class="form-control"  name="property_price" value="<?php print $property_price;?>">
				</p>
			</div>
		</div>
		<div class="col-md-12">	
			<div class="col-md-3 dashboard_chapter_label">           
                <p>
                   <label for="price_asking_for"><?php esc_html_e('Price you are asking','wpestate');?></label>
                 </p>
            </div>
			<div class="col-md-6">
				<p>
					
					<input type="text" id="price_asking_for" size="40" placeholder="Enter price you are asking for" class="form-control"  name="price_asking_for" value="<?php print $price_asking_for;?>">
				</p>
			</div>
		</div>

        <div class="col-md-12">
            <div class="col-md-3 dashboard_chapter_label" style="display: none;">
                <p>
                    <label for="prop_category"><?php esc_html_e('*Category and Listed In/Room Type','wpestate');?></label>
                </p>
            </div>

            <div class="col-md-3" style="display: none;">
                <p>
                    <label for="prop_category"><?php esc_html_e('*Category (mandatory)','wpestate');?></label>
                    <?php 
                        $args=array(
                                'class'       => 'select-submit2',
                                'hide_empty'  => false,
                                'selected'    => $prop_category_selected,
                                'name'        => 'prop_category',
                                'id'          => 'prop_category_submit',
                                'orderby'     => 'NAME',
                                'order'       => 'ASC',
                                'show_option_none'   => esc_html__( 'None','wpestate'),
                                'taxonomy'    => 'property_category',
                                'hierarchical'=> true
                            );
                        wp_dropdown_categories( $args ); ?>

                </p>
            </div>



            <div class="col-md-3" style="display: none;">
                <p>
                    <label for="prop_action_category"> <?php esc_html_e('*Listed In/Room Type (mandatory)','wpestate'); $prop_action_category;?></label>
                    <?php 
                    $args=array(
                            'class'       => 'select-submit2',
                            'hide_empty'  => false,
                            'selected'    => $prop_action_category_selected,
                            'name'        => 'prop_action_category',
                            'id'          => 'prop_action_category_submit',
                            'orderby'     => 'NAME',
                            'order'       => 'ASC',
                            'show_option_none'   => esc_html__( 'None','wpestate'),
                            'taxonomy'    => 'property_action_category',
                            'hierarchical'=> true
                        );

                       wp_dropdown_categories( $args );  ?>
                </p>       
            </div>

        </div>    




        <div class="col-md-12"> 

            <div class="col-md-3 dashboard_chapter_label" style="display: none;">
                <p>
                    <label for="guest_no"><?php esc_html_e('*Guest No','wpestate');?></label>
                </p>
            </div>
            <div class="col-md-3" style="display: none;">
                <p>
                    <label for="guest_no"><?php esc_html_e('*Guest No (mandatory)','wpestate');?></label>
                    <select id="guest_no" name="guest_no">
                        <?php 
                        $guest_dropdown_no                    =   intval   ( get_option('wp_estate_guest_dropdown_no','') );
     
                        for($i=0; $i <= $guest_dropdown_no; $i++) {
                            print '<option value="'.$i.'" ';
                                if ( $guestnumber==$i){
                                    print ' selected="selected" ';
                                }
                            print '>'.$i.'</option>';
                        } ?>
                    </select>    
                </p>
            </div>
        </div>  

            



        <?php  if($show_adv_search_general=='no'){
        ?>  
        <div class="col-md-12"> 
            <div class="col-md-3 dashboard_chapter_label"> 
                <label for="property_country"><?php esc_html_e('Country','wpestate');?></label>
            </div>
            <div class="col-md-3 property_country"> 
                <label for="property_country"><?php esc_html_e('Country','wpestate');?></label>
                <?php print wpestate_country_list( '' ); ?>
            </div>
        </div>
        <?php    
        }?>

        <div class="col-md-12">
            <div class="col-md-3 dashboard_chapter_label"> 
                <label for="property_description"><?php esc_html_e('Property Description','wpestate');?></label>
            </div>

            <div class="col-md-6"> 
                <textarea  rows="4" id="property_description" name="property_description"  class="advanced_select  form-control" placeholder="<?php esc_html_e('Describe your property','wpestate');?>"><?php echo $property_description; ?></textarea>
            </div>
        </div>

        <div class="col-md-12"> 
            <input style="float:left;" type="checkbox" class="form-control" value="1"  id="instant_booking" name="instant_booking" <?php print $instant_booking; ?> >
            <label style="display: inline;" for="instant_booking"><?php esc_html_e('Allow instant booking? If checked, you will not have the option to reject a booking request.','wpestate');?></label>
        </div>

            <?php
            if ( !is_user_logged_in() ) { 
                print '<input type="hidden" name="pointblank" value="1">';  
            }else{
                print '<input type="hidden" name="pointblank" value="0">';   
            }
        ?>
    </div>
    <div class="col-md-12" style="display: inline-block;">   
        <?php if ( is_user_logged_in() ) {    ?>
            <input type="submit"  class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button"  id="form_submit_1" value="<?php esc_html_e('Continue', 'wpestate') ?>" />
        <?php }else{ ?>
            <input type="submit"  class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button externalsubmit"  id="form_submit_1" value="<?php esc_html_e('Continue', 'wpestate') ?>" />

        <?php } ?>
    </div>


    </div>
</div>

    <input type="hidden" id="security-login-submit" name="security-login-submit" value="<?php echo estate_create_onetime_nonce( 'submit_front_ajax_nonce' );?>">
 
        
<?php 

print ' <input type="hidden" name="estatenonce" value="'.sh_create_onetime_nonce( 'thisestate' ).'"/>';

wp_nonce_field('submit_new_estate','new_estate'); 

function sh_create_onetime_nonce($action = -1) {
    $time = time();
    $nonce = wp_create_nonce($time.$action);
    return $nonce . '-' . $time;
}

?>
    
<?php if ( is_user_logged_in() ) {    ?>
</form>  
<?php }else{ 
    echo '<span class="next_submit_page_first_step">'.esc_html__('You must Login / Register in the modal form that shows after you press the Continue button or else your data will be lost. ','wpestate').'</span>';?>
</div>    
<?php } ?>